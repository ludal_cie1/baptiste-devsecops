# Utilise une image Python officielle en tant qu'image de base
FROM python:3.12-alpine

# Définit le répertoire de travail dans le conteneur
WORKDIR /app

# Copie les fichiers nécessaires dans le conteneur
COPY app.py ./
COPY test.csv ./
COPY templates ./templates

# Installe les dépendances de l'application
RUN pip install flask
RUN pip install plotly


# Expose le port sur lequel l'application s'exécute
EXPOSE 5000

# Commande pour exécuter l'application Flask dans le conteneur
CMD ["python", "app.py"]
