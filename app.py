from flask import Flask, render_template, jsonify
import csv
from collections import Counter

app = Flask(__name__)

def charger_donnees_logement():
    try:
        with open('test.csv', 'r', newline='') as fichier_csv:
            lecteur_csv = csv.DictReader(fichier_csv)
            donnees_logement = {}
            for ligne in lecteur_csv:
                nom_region = ligne['nom_region']
                nombre_logement = int(ligne['nombre_de_logements'])
                if nom_region in donnees_logement:
                    donnees_logement[nom_region] += nombre_logement
                else:
                    donnees_logement[nom_region] = nombre_logement
            return donnees_logement
    except FileNotFoundError:
        print("Le fichier spécifié est introuvable .")
        return {}
    except Exception as e:
        print(f"Une erreur s'est produite : {e}")
        return {}
    
def charger_donnees_chomage():
    try:
        with open('test.csv', 'r', newline='', encoding='utf-8') as fichier_csv:
            lecteur_csv = csv.DictReader(fichier_csv)
            donnees_chomage = {}
            for ligne in lecteur_csv:
                nom_region = ligne['nom_region']
                annee = ligne['annee_publication']
                taux_chomage = float(ligne['chomage'])
                if nom_region in donnees_chomage:
                    if annee in donnees_chomage[nom_region]:
                        donnees_chomage[nom_region][annee].append(taux_chomage)
                    else:
                        donnees_chomage[nom_region][annee] = [taux_chomage]
                else:
                    if annee in donnees_chomage.get(nom_region, {}):
                        donnees_chomage[nom_region][annee].append(taux_chomage)
                    else:
                        donnees_chomage[nom_region] = {annee: [taux_chomage]}
            return donnees_chomage
    except FileNotFoundError:
        print("Le fichier spécifié est introuvable.")
        return {}
    except Exception as e:
        print(f"Une erreur s'est produite : {e}")
        return {}
    
def charger_donnees_habitants():
    try:
        with open('test.csv', 'r', newline='') as fichier_csv:
            lecteur_csv = csv.DictReader(fichier_csv)
            donnees_habitants = {}
            for ligne in lecteur_csv:
                nom_region = ligne['nom_region']
                annee = ligne['annee_publication']
                nombre_habitants = float(ligne['habitant'])
                if nom_region in donnees_habitants:
                    if annee in donnees_habitants[nom_region]:
                        donnees_habitants[nom_region][annee].append(nombre_habitants)
                    else:
                        donnees_habitants[nom_region][annee] = [nombre_habitants]
                else:
                    if annee in donnees_habitants.get(nom_region, {}):
                        donnees_habitants[nom_region][annee].append(nombre_habitants)
                    else:
                        donnees_habitants[nom_region] = {annee: [nombre_habitants]}
            return donnees_habitants
    except FileNotFoundError:
        print("Le fichier spécifié est introuvable.")
        return {}
    except Exception as e:
        print(f"Une erreur s'est produite : {e}")
        return {}
    
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/donnees')
def obtenir_donnees():
    donnees = compter_occurrences_regions()
    return jsonify(donnees)
    
@app.route('/donnees_logement')
def obtenir_donnees_logement():
    donnees_logement = charger_donnees_logement()
    return jsonify(donnees_logement)

@app.route('/donnees_chomage')
def obtenir_donnees_chomage():
    donnees_chomage = charger_donnees_chomage()
    return jsonify(donnees_chomage)

@app.route('/donnees_habitant')
def obtenir_donnees_habitant():
    donnees_habitant = charger_donnees_habitants()
    return jsonify(donnees_habitant)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
